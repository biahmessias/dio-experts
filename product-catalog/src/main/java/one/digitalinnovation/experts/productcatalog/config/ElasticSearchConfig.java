package one.digitalinnovation.experts.productcatalog.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "digitalinnovation.one.experts.productcatalog.repository")
public class ElasticSearchConfig  {


}
